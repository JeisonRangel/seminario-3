import Vue from 'vue'
import VueRouter from 'vue-router'
import Inicio from '../views/Inicio.vue'
import LecturaTiempoReal from '../views/LecturaTiempoReal.vue'
import HistorialLecturas from '../views/HistorialLecturas.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Inicio',
    component: Inicio
  },
  {
    path: '/ltreal',
    name: 'LecturaTiempoReal',
    component: LecturaTiempoReal
  },
  {
    path: '/historial',
    name: 'HistorialLecturas',
    component: HistorialLecturas
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
